import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getPayments = async () => {
//  axios.get(`${userServiceBaseUrl}/payments`)
//   .then(response => {
//     return response
//   })
//   .then(data => {
//     console.log(data)
//   })
//   .catch(error => {
//     console.log(error.response.data.error)
//   })
// };
const { data } = await axios.get(`${userServiceBaseUrl}/payments`);
  return data;
};

export const createPayment = async ({ uuid, applicationUuid, requestedAmount }) => {
  const { data } = await axios.post(`${userServiceBaseUrl}/payments`, {
    uuid,
    applicationUuid,
    paymentAmount: requestedAmount,
  })
  .catch(error => {
    console.log(error.response.data.errror)
  });
  return data;
  // 2nd challenge attempt
  
//   const { data } =
//   await axios.post(`${userServiceBaseUrl}/payments`, {
//       uuid,
//       applicationUuid,
//       paymentAmount: requestedAmount,
//     })
//   .then(response => {
//     data = response
//   })
//   .then(data => {
//     console.log(data)
//   })
//   .catch(error => {
//     console.log(error.response.data.error)
//   })
// return { data }
};
